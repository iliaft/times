let html = "<span>{{data}}</span>";
let template = Handlebars.compile(html);
// clock in head
setInterval(function() {
    let current_date = moment().format('LTS');
    $("#current-date").html(current_date);
}, 1000);

$('#datepicker').datepicker({
    dateFormat:'yy-mm-dd',
    onSelect: function (dateText, inst) {
        // getting data from moment
        let selected_date = moment(dateText).format('LL');
        let from_now_date = moment(dateText).fromNow();
        // context for templates
        let context1 = {data: selected_date};
        let context2 = {data: from_now_date};
        // adding compiled html
        $("#selected").html(template(context1));
        $("#from").html(template(context2));
    }
});
// analog clock template generation start -------------------------------->
let clockContext = {
    clock: [
        {name: 'Current'},
        {name: 'New York'},
        {name: 'Tokyo'}
    ]
};
Handlebars.registerHelper('clocks', function(items, options) {
    let out = "<div class='flex-container'>";
    for(let i=0, l=items.length; i<l; i++) {
        out = out + "<div class='clock__main'><h2>" + options.fn(items[i])+" time :</h2><div id='drawArea" +
            i + "' class='clock__draw'></div></div>";
    }
        return out + "</div>";
});

let source   = "{{#clocks clock}}{{name}} {{/clocks}}";
let clockTemplate = Handlebars.compile(source);
$("#clock-list").html(clockTemplate(clockContext));
//   end------------------------------------------------------------------->
