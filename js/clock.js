let cx; // center x of clock
let cy; // center y of clock
let gContainerId;
let gCanvasId;
let gTopGroupId;
let radius; // radius of clock
let granularity = 30;
// Redraw niddle with length and angle
function ReDrawNiddle(niddle, angle) {
    let name = niddle.slice(-1);
    let svg = d3.select("#drawArea" + name + "_topGroup");
    let line = svg.select("#" + niddle);
    let savedX2 = parseFloat(line.attr("x2Saved"));
    let savedY2 = parseFloat(line.attr("y2Saved"));
    let lineLength = parseFloat(line.attr("lineLength"));
    offsetX = savedX2 - (lineLength * Math.cos(angle + Math.PI / 2));
    offsetY = savedY2 - (lineLength * Math.sin(angle + Math.PI / 2));
    line
        .transition()
        .duration(100)
        .attr("x2", offsetX)
        .attr("y2", offsetY);
}
// Create dial by putting circle, dots and numbers 12, 3, 6 and 9
function DrawDots() {
    for (let a = 0; a < 60; ++a) {
        let t = a * Math.PI / granularity;
        let lineLength = radius - 10;
        let offsetX = cx - (lineLength * Math.cos(t + Math.PI / 2));
        let offsetY = cy - (lineLength * Math.sin(t + Math.PI / 2));
        let svg = d3.select("#" + gTopGroupId);
        svg.append("circle")
            .attr("id", "second_pos_" + a)
            .attr("cx", offsetX)
            .attr("cy", offsetY)
            .attr("r", function () {
                if (a % 5 == 0)
                    return 2;
                else return 1;
            })
            .attr("saver", function () {
                if (a % 5 == 0)
                    return 2;
                else return 1;
            })
            .attr('fill', 'black');
        let fontsize = 20;
        if (a == 0) {
            svg.append("text")
                .attr("x", offsetX)
                .attr("y", offsetY + fontsize + 5)
                .attr("font-size", fontsize + 'px')
                .attr("text-anchor", "middle")
                .attr("fill", "black")
                .text("12");
        }
        else if (a == 15) {
            svg.append("text")
                .attr("x", offsetX - fontsize / 2)
                .attr("y", offsetY + fontsize / 2)
                .attr("font-size", fontsize + 'px')
                .attr("text-anchor", "middle")
                .attr("fill", "black")
                .text("3");
        }
        else if (a == 30) {
            svg.append("text")
                .attr("x", offsetX)
                .attr("y", offsetY - 5)
                .attr("font-size", fontsize + 'px')
                .attr("text-anchor", "middle")
                .attr("fill", "black")
                .text("6");
        }
        else if (a == 45) {
            svg.append("text")
                .attr("x", offsetX + fontsize / 2)
                .attr("y", offsetY + fontsize / 2)
                .attr("font-size", fontsize + 'px')
                .attr("text-anchor", "middle")
                .attr("fill", "black")
                .text("9");
        }
    }
}
// Draw svg
function InitializeSvg(containerId, id) {
    let height = document.getElementById(containerId).clientHeight;
    let width = document.getElementById(containerId).clientWidth;
    let hourN = "hourNiddle"+id;
    let minuteN = "minuteNiddle"+id;
    let secondN = "secondNiddle"+id;

    gContainerId = containerId;
    gCanvasId = containerId + '_canvas';
    gTopGroupId = containerId + '_topGroup';
    svg = d3.select("#" + containerId).append("svg")
        .attr("id", gCanvasId)
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("id", gTopGroupId)
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", width)
        .attr("height", height)
    ;
    lineLength = (height / 2) - 110;
    cx = width / 2;
    cy = height / 2;
    let secondNiddle = svg.append("line")
        .attr("id", secondN)
        .attr("x1", cx)
        .attr("y1", cy)
        .attr("x2", cx)
        .attr("y2", (cy - lineLength))
        .attr("lineLength", lineLength)
        .attr("x2Saved", cx)
        .attr("y2Saved", (cy))
        .attr("stroke-width", 2)
        .attr("stroke", "black")
    ;
    lineLength = (height / 2) - 140;
    let minuteNiddle = svg.append("line")
        .attr("id", minuteN)
        .attr("x1", cx)
        .attr("y1", cy)
        .attr("x2", cx)
        .attr("y2", (cy - lineLength))
        .attr("lineLength", lineLength)
        .attr("x2Saved", cx)
        .attr("y2Saved", (cy))
        .attr("stroke-width", 4)
        .attr("stroke", "red")
    ;
    lineLength = (height / 2) - 160;
    let hourNiddle = svg.append("line")
        .attr("id", hourN)
        .attr("x1", cx)
        .attr("y1", cy)
        .attr("x2", cx)
        .attr("y2", (cy - lineLength))
        .attr("lineLength", lineLength)
        .attr("x2Saved", cx)
        .attr("y2Saved", (cy))
        .attr("stroke-width", 6)
        .attr("stroke", "green")
    ;
    let circle = svg.append("circle")
        .attr("id", "centerBall")
        .attr("cx", cx)
        .attr("cy", cy)
        .attr("r", 5)
        .attr("fill", "black")
    ;
    radius = (height / 2) - 90;
    let circle2 = svg.append("circle")
        .attr("id", "outerCircle")
        .attr("cx", cx)
        .attr("cy", cy)
        .attr("r", radius)
        .attr("stroke-width", 1)
        .attr('stroke', "black")
        .attr('fill', 'none')
    ;
    DrawDots();
}
// Launchs clock
function goClock(id, timeZone) {
    InitializeSvg("drawArea"+id, id);
    setInterval(function(){
        let hourNi = "hourNiddle"+id;
        let minuteNi = "minuteNiddle"+id;
        let secondNi = "secondNiddle"+id;

        let d = new Date();
        seconds = d.getSeconds();
        minutes = d.getMinutes();
        hours = (d.getHours()+timeZone) % 12;
        ReDrawNiddle(secondNi, seconds * Math.PI / granularity/3);
        ReDrawNiddle(minuteNi, minutes * Math.PI / granularity);
        let hourPos = hours * 10;
        hourPos += (minutes * 10) / 60;
        ReDrawNiddle(hourNi, hourPos * Math.PI / 60);

        ++seconds;
        if (seconds >= 60) {
            seconds = 0;
            ++minutes;
            if (minutes % 5 == 0) {
                if (minutes >= 60) {
                    minutes = 0;
                    ++hours;
                }
                if (hours >= 12)
                    hours = 0;
                let hourPos = hours * 10;
                hourPos += (minutes * 10) / 60;
                ReDrawNiddle(hourNi, hourPos * Math.PI / 60);
            }
            ReDrawNiddle(minuteNi, minutes * Math.PI / granularity);
        }
        ReDrawNiddle(secondNi, seconds * Math.PI / granularity);
    }, 1000);
}
// Calls clock, first parameter - id of drawArea, second - timezone offset
goClock(0, 0);
goClock(1, -6);
goClock(2, 7);
